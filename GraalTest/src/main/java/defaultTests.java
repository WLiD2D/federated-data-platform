import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.io.dlp.DlgpParser;
import fr.lirmm.graphik.graal.io.dlp.DlgpWriter;
import fr.lirmm.graphik.util.Prefix;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Set;

public class defaultTests {
    public static void main(String[] args) throws Exception {
        DlgpWriter writer = new DlgpWriter();
        writer.write(new Prefix("class", "http://unisa.edu.au/KSE.owl/class#"));
        writer.write(new Prefix("attribute", "http://unisa.edu.au/KSE.owl/attribute#"));
        writer.write(new Prefix("relationship", "http://unisa.edu.au/KSE.owl/relationship#"));
        writer.write(new Prefix("", ""));
        ConjunctiveQuery lastConjunctiveQuery = DlgpParser.parseQuery("@prefix class: <http://unisa.edu.au/KSE.owl/class#>\n"
                + "        @prefix attribute: <http://unisa.edu.au/KSE.owl/attribute#>\n"
                + "        @prefix relationship: <http://unisa.edu.au/KSE.owl/relationship#>\n"
                + "        @prefix : <>\n"
                + "?(ID,Type) :- class:Investigator(ID,Type),Type = <investigator>.");
        Pair<ConjunctiveQuery, Substitution> pair = EqualityUtils.processEquality(lastConjunctiveQuery);
        lastConjunctiveQuery = pair.getLeft();
        Substitution sub = pair.getRight();
        writer.write("sub: \n");
        writer.write(sub);
        Set terms = sub.getTerms();
        Set values = sub.getValues();
        writer.write("Terms: \n");
        Object[] termArray = terms.toArray();
        for (Object o : termArray) {
            writer.write(o);
        }
        writer.write("Values: \n");
        Object[] valueArray = values.toArray();
        for (Object o : valueArray) {
            writer.write(o);
        }
        writer.write("lastConjunctiveQuery: \n");
        writer.write(lastConjunctiveQuery);
        writer.close();

    }
}
