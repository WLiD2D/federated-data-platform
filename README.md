# README #

The law enforcement environment has been dramatically changed in the past 20 years. In order to catch up with these changes and meet the challenges in the era of big data, the objective of the Integrated Law Enforcement project is to develop, integrate and demonstrate state-of-the-art capabilities in federated data management, entity resolution and federated analytics to provide law enforcement agencies and analysts with uniform and timely access to integrated information and analytical outcomes. To meet the objective, a set of cutting edge technologies will be comprised to effectively link all available data among internal information systems and external data sources and reveals the relationships among data segments of various natures and types, so that an overall image around the topic of interest can be quickly formed. 

### What is this repository for? ###

* This is the repository for codes that can facilitate a quickstart for building the federated data platform.

### Installation for Bitbucket Federated Data Platform Repository ###

* All components represents different modules of the federated Data Platform. As all components are implemented using maven+springboot configuration and programmed using intellij IDE, therefore, it is quite straight forward when load the code on your local developing IDE and compile. Despite of graal.1.3.1 libraries, all other dependencies can be automatically compiled using Maven.

### Components ###

1. Graal source code
Graal is a Java toolkit dedicated to querying knowledge bases within the framework of existential rules, aka Datalog. We leverage several revised Graal components and functions to conduct ontology based federated search (knowledge search) in the FDP. The code of graal is located in Workspace/IntellijGit/graalSource/graal-graal-1.3.1 folder, and each module was built as Maven projects. As several places has been revised to eliminate bugs and extend existing functions, it is recommended that all maintenance/updates in the future based on this version of Graal instead of the original version.
To use the graal.1.3.1 library, simply import the folder as an external library. Or for pack the subfolders and include the jar files as dependencies of the project.
For detailed documentation of Graal, please refer to the original Graal homepage at https://graphik-team.github.io/graal/

2. Federated data platform
The FDP is implemented majorly using maven + springboot setup. Detailed design and functionality provided by the FDP is described in paper FEDSA:A Data Federation Platform for Law Enforcement Management.

    * 2.1.	ETL and data ingestion modules
We have implemented the initial batch ingestion and pipeline update modules in the initial POC building phase of FDP. Related code is stored in the PromisDataIngestion module, PromisUpdatePipeLine module, and in Demo module which is for the demo on 15/11/2016. Basically, what it does is to compose elasticsearch documents based on Promis3 database and ingest into the elasticsearch indexes.
We also have some ETL code for preparing test database allocated at FederationEngine/src/main/java/DataPreparation folder, which contains functions for test data generation and ingestion to relational databases using JDBC.

    * 2.2.	Federated search modules
The main function and configuration portal of the federated search module is located in edu.unisa.ILE.FSA/EnginePortal/FDEApplication.java file. This file contains access point information for all data sources including ElasticSearch, Promis, HDFS, OBDA sources and  also internal cache and mapping repository.
FDP provides RESTful services to external users. 
In general, 1) For standard services, the control flow is FDEApplication->QueryController->KeyFunctions.query()->KeyFunctions.functionName()->CallserviceFunctions.querySource()->Adapter.send()->Specific function in specific adapters. 
    2) For findEntities service specifically, there is a QueryParser component designed to parse the query criteria (in MongoDB syntax) into a syntax tree structure, and further translated into SQL queries for external data sources in relational databases.
    3) For knowledge query services, the control flow is FDEApplication->OBDAController->OBDAFunctions.knowledgeQueryV2()->FDAFunctions.federatedKnowledgeQuery(). 
    4) For mapping repository operations, the control flow is FDEApplication->MRController->MRFunctions.conduct()->MongoFunctions. 
    5) For MRCache operations, the control flow is FDEApplication->MRCache.

    * 2.3.	One-box-search module 
The One-box-search module is implemented on top of the D2DCRC template from the Apostle project using reactjs + springboot server + maven. The purpose is to demonstrate the capability of FDP in a more user-friendly way. Demo of how this module is used and how result is shown can be found in report 20190404-final-_FDP_UniSA located in the Reports and Publication/reports folder.

    * 2.4.	Workflow service module
The Workflow module is implemented as an extension on top of the provided services of FDP to allow flexible adjustment and creation of new federated search services to meet user�s rapid changing searching requirement. This module is implemented using camunda workflow management tool + springboot server, where the workflow is designed using camunda graphical workflow editing tool, where the workflow components can be linked to a generic java function that calls the standard services of FDP. It also comes with a findNearByEntities.bpmn example, which calls the findEntities service of FDP and reach Entities of given type K steps away.

3. Other modules for testing code
Some other modules are created during the initial learning and testing phase for each technology including ElasticSearch module, GraalTest module, HDFS module, Junit module, PostgreSQL module, RMQ module (rabbit MQ), RMQListener module, and Spark module. These modules are for test purposes but also contains some ready to use functions.

### Contact ###

ACRC, UniSA

Mawson lakes, SA 5067

Ph: +61 (08) 8302 3582 

ACRC.Enquiries@unisa.edu.au

www.unisa.edu.au

[testing platform install]: https://d2dcrc.atlassian.net/wiki/download/attachments/45907984/Initial%20Federated%20Process%20Infrastructure.docx?version=1&modificationDate=1476335556307&api=v2