import React from 'react'

/**
 * About page component
 */
const Ontology = React.createClass({

    /**
     * Renders the About page
     * @returns {XML} the page content
     */
    render() {
        console.log("ontology");
        return (
            <form>
                <div>
                    <div className="col-sm-12">
                        <img src={require("components/onto/ontology.png")} className="logo"/>
                    </div>

                    <div>
                        <h3>Format of query:</h3>
                        <br/>
                        <h4>Query Structure</h4>
                        <ul>
                            <li>?(Attribute 1, Attribute 2, ..., Attribute N) :- Atom1, Atom2, ..., AtomN</li>
                            <li>On the left side of operator ":-", Attribute 1 to Attribute N represent the output attributes that the users want to get from the query</li>
                            <li>On the right side of operator ":-", Atom 1 to Atom N represent the Ontology items that are involved including class, attribute and relationship</li>
                        </ul>
                        <br/>
                        <h4>Each Atom</h4>
                        <ul>
                            <li>Atom structure: Predicate(Augment1, Augment2, ..., Augment M)</li>
                            <li>In each atom, the Predicate represents the name of the ontology item, the format of each predicate is type:predicateId, for example, class:Person, relationship:Person_Case, attribute:Person_Title, etc.</li>
                            <li>For each predicate, if the type is class, the predicateId is ontology item's name; If the type is attribute, the predicateId is className_attributeName; If the type is relationship, the predicateId is class1Name_class2Name;</li>
                            <li>**Temporary** For each Atom, the augments are also following a certain format</li>
                            <li>If the predicate is of type class, the augments are (ID,Type);If the predicate is of type attribute, the augments are (ID, AttributeName,Type);If the predicate is of type relationship, the augments are (class1ID, class2ID, relationshipType);</li>
                            <li>Each augment can be variable (with uppercase initial), or constant (enclosed in brackets &lt;&gt;)</li>
                        </ul>
                        <br/>
                    </div>
                </div>
            </form>
        );
    }
});

export default Ontology;
