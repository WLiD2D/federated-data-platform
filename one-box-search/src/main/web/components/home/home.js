import React from 'react';
import axios from 'axios'

import SearchResultDisplay from 'components/entitysearch/SearchResultDisplay.js'

/**
 * Home page component
 */

class Home extends React.Component{

    /**
     * Set the initial state for the Home page
     * @returns {Object} the initial state*/

    constructor(props) {
        super(props);
        this.state = {
            vms: false,
            promis: false,
            cms: false,
            lei: false,
            hdfs: false,
            person: false,
            location: false,
            case: false,
            typeAll: false,
            service: '',
            update: false,
            limit: '',
            criteria: {},
            data:null,
            latency:0
        };
        this.handleChange = this.handleChange.bind(this);
        this.submit = this.submit.bind(this);
        this.choseAll = this.choseAll.bind(this);
    }

    handleChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        const newState = {};
        newState[name] = value;
        this.setState(newState);
        console.log(name, ' -> ', value);
    }

    choseAll(event) {
        this.handleChange(event);
        let checked = event.target.checked;
        let names = ['person', 'location', 'case'];
        names.map((name)=>{
            this.setState({
                [name]:checked
            });
        })
    }

    /**
     * Renders the Home page
     * @returns {XML} the page content
     */

    submit(event) {
        this.doAxiosQuery(this.state);
    }

    doAxiosQuery(props) {
        console.log('doAxiosQuery');
        let query = this.createQuery(props);
        let start = Date.now();
        axios.post('http://localhost:8091/query', query)
            .then(function (response) {
                let time = Date.now() - start;
                console.log("time:"+time)
                this.setState({
                    update:true,
                    data:response.data,
                    latency:time
                });
                this.setState({update:false});
            }.bind(this)).catch(function (error) {
            console.log(error);
            alert(error);
        });

    }

    createQuery(props){
        let types = [];
        let allTypes = ['person', 'location', 'case'];
        allTypes.map((name)=>{
            if (this.state[name] === true) {
                types.push(name);
            }
        });
        console.log(types);



        let sources = [];
        let allSources = ['vms','cms','','promis', 'lei', 'hdfs'];
        allSources.map((name)=>{
            if (this.state[name] === true) {
                sources.push(name);
            }
        });
        console.log(sources);
        console.log('props.criteria');
        console.log(props.criteria);
        console.log("limit:"+props.limit);

        let limit;
        if (props.limit === undefined || props.limit === ''){
            limit = '100';
        } else {
            limit = props.limit;
        }

        if (props.service === 'Federated Knowledge Query') {
            let queryString = props.criteria + '.';
            let query = {
                "query": {
                    "window":{"limit":limit},
                    "filter": {
                        "query": queryString
                    }
                },
                "control": {
                    "operation": "knowledgeQueryV2",
                    "sources": sources },
                "credentials": {
                    "credentiallist": [
                        { "vms": {} },
                        { "cms": {} }]
                }
            };
            console.log(query);
            return query;
        }

        if (props.service === 'Knowledge Query') {
            let queryString = props.criteria + '.';
            let query = {
                "query": {
                    "window":{"limit":limit},
                    "filter": {
                        "query": queryString
                    }
                },
                "control": {
                    "operation": "knowledgeQuery",
                    "sources": [] },
                "credentials": {
                    "credentiallist": [
                        {"lei": { "username": "unisaile", "password": "unisaile" } },
                        { "promis": { "username": "unisa", "password": "unisa" } },
                        { "obdapostgresql": { "username": "unisa", "password": "unisa" } },
                        { "obdasqlite": {} },
                        { "hdfs": { "username": "ubuntu", "password": "ubuntu" } }]
                }
            };
            console.log(query);
            return query;
        }

        if (props.service === 'Attribute Query'){
            let criteriaJSON = undefined;
            try {
                criteriaJSON = JSON.parse('{' + props.criteria + '}');
            } catch (error) {
                console.log(error);
                alert(error);
            }

            let query = {
                "query":{
                    "scope":types,
                    "output":{"project":{}},
                    "window":{"limit":limit},
                    "filter":criteriaJSON},
                "control":{
                    "operation":"findEntities",
                    "sources":sources},
                "credentials":{
                    "credentiallist":[
                        {"es":{"username":"unisaile","password":"unisaile"}},
                        {"lei":{"username":"unisaile","password":"unisaile"}},
                        {"promis":{"username":"unisa","password":"unisa"}}]
                }
            };
            return query;
        }
        if (props.service === 'Keyword Query'){
            let criteriaJSON = undefined;
            try {
                criteriaJSON = JSON.parse('{"keywords":[' + props.criteria + '],"mode":"all"}');
            } catch (error) {
                console.log(error);
                alert(error);
            }

            let query = {
                "query":{
                    "scope":types,
                    "output":{
                        "project":{}},
                    "window":{"limit":limit},
                    "filter":criteriaJSON},
                "control":{
                    "operation":"findEntitiesByKeyword",
                    "sources":sources},
                "credentials":{
                    "credentiallist":[
                        {"es":{"username":"unisaile","password":"unisaile"}},
                        {"promis":{"username":"unisa","password":"unisa"}},
                        {"lei":{"username":"unisa","password":"unisa"}}]}
            };
            return query;
        }
        if (props.service === 'Get Text Content'){
            let queryString = undefined;
            try {
                queryString = JSON.parse('{"id":"' + props.criteria + '"}');
            } catch (error) {
                console.log(error);
                alert(error);
            }

            let query = {
                "query":{
                    "filter":queryString},
                "control":{
                    "operation":"getTextContent",
                    "sources":["hdfs"]},
                "credentials":{
                    "credentiallist":[
                        {"es":{"username":"unisaile","password":"unisaile"}},
                        {"lei":{"username":"unisaile","password":"unisaile"}},
                        {"promis":{"username":"unisa","password":"unisa"}},
                        {"hdfs":{"username":"ubuntu","password":"ubuntu"}}
                    ]}
            };
            return query;
        }
        if (props.service === 'Get Binary Content'){
            let queryString = undefined;
            try {
                queryString = JSON.parse('{"id":"' + props.criteria + '"}');
            } catch (error) {
                console.log(error);
                alert(error);
            }

            let query = {
                "query":{
                    "filter":queryString},
                "control":{
                    "operation":"getBinaryContent",
                    "sources":["hdfs"]},
                "credentials":{
                    "credentiallist":[
                        {"lei":{"username":"unisaile","password":"unisaile"}},
                        {"promis":{"username":"unisa","password":"unisa"}},
                        {"hdfs":{"username":"ubuntu","password":"ubuntu"}}
                    ]}
            };
            return query;
        }
    }

    render() {
        console.log('rendering home.js');
        return (
            <form>
                <div>
                    <br/><br/><br/><br/>
                    <div className="col-sm-12 col-sm-offset-4">
                        <img src={require("components/home/unisa_logo.png")} className="logo"/>
                    </div>
                    <div className="col-sm-12">
                        <br/><br/>
                        <div className="col-sm-1"></div>
                        <div className="col-sm-1">
                            <strong>Sources:</strong>
                        </div>

                        <div className="col-sm-1">
                            <label className="checkbox-inline">
                                <input type="checkbox" name="cms" onChange={this.handleChange} checked={this.state.cms}/>
                                <strong>CMS</strong>
                            </label>
                        </div>
                        <div className="col-sm-1"></div>

                        <div className="col-sm-1">
                            <label className="checkbox-inline">
                                <input type="checkbox" name="vms" onChange={this.handleChange} checked={this.state.vms}/>
                                <strong>VMS</strong>
                            </label>
                        </div>
                        <div className="col-sm-1"></div>

                        <div className="col-sm-1">
                            <label className="checkbox-inline">
                                <input type="checkbox" name="promis" onChange={this.handleChange} checked={this.state.promis}/>
                                <strong>Promis</strong>
                            </label>
                        </div>
                        <div className="col-sm-1"></div>

                        <div className="col-sm-1">
                            <label className="checkbox-inline">
                                <input type="checkbox" name="lei" onChange={this.handleChange} checked={this.state.lei}/>
                                <strong>LEI</strong>
                            </label>
                        </div>
                        <div className="col-sm-1"></div>

                        <div className="col-sm-1">
                            <label className="checkbox-inline">
                                <input type="checkbox" name="hdfs" onChange={this.handleChange} checked={this.state.hdfs}/>
                                <strong>HDFS</strong>
                            </label>
                        </div>
                    </div>

                    <div className="col-sm-12">
                        <div className="col-sm-1"></div>
                        <div className="col-sm-1">
                            <strong>Types:</strong>
                        </div>
                        <div className="col-sm-1">
                            <label className="checkbox-inline">
                                <input type="checkbox" name="person" onChange={this.handleChange} checked={this.state.person}/>
                                <strong>Person</strong>
                            </label>
                        </div>
                        <div className="col-sm-1"></div>

                        <div className="col-sm-1">
                            <label className="checkbox-inline">
                                <input type="checkbox" name="location" onChange={this.handleChange} checked={this.state.location}/>
                                <strong>Location</strong>
                            </label>
                        </div>
                        <div className="col-sm-1"></div>

                        <div className="col-sm-1">
                            <label className="checkbox-inline">
                                <input type="checkbox" name="case" onChange={this.handleChange} checked={this.state.case}/>
                                <strong>Case</strong>
                            </label>
                        </div>
                        <div className="col-sm-1"></div>

                        <div className="col-sm-1">
                            <label className="checkbox-inline">
                                <input type="checkbox" name="typeAll" onChange={this.choseAll} checked={this.state.typeAll}/>
                                <strong>All</strong>
                            </label>
                        </div>
                    </div>

                    <div className="col-sm-12">
                        <div className="col-sm-2">
                            <select className="form-control" name="service" onChange={this.handleChange}>
                                <option>Select service type</option>
                                <option>Attribute Query</option>
                                <option>Federated Knowledge Query</option>
                                <option>Keyword Query</option>
                                <option>Get Text Content</option>
                                <option>Get Binary Content</option>
                            </select>
                        </div>
                        <div className="col-sm-1">
                            <input type="text" className="form-control" placeholder="Limit:100" name="limit" onChange={this.handleChange}/>
                        </div>
                        <div className="input-group col-sm-9">
                            <input type="text" className="form-control" placeholder="Input search criteria" name="criteria" onChange={this.handleChange}/>
                            <span className="input-group-btn">
                                    <button className="btn btn-primary" type="button" name="submit" onClick={this.submit}>Go!</button>
                                </span>
                        </div>
                        <br/><br/><br/><br/>
                    </div>
                </div>
                <div>
                    <div className="col-sm-12">
                        <SearchResultDisplay data = {this.state.data} update = {this.state.update} latency = {this.state.latency}/>
                    </div>
                </div>
            </form>
        );
    }
}

export default Home;