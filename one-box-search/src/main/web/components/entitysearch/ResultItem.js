import React from 'react'


class ResultItem extends React.Component{

    constructor(props) {
        super(props);
        this.state = {};
    }

    searchFromJSONArray(array, key){
        for(let i = 0; i<array.length; i++){
            if (array[i].hasOwnProperty(key)){
                return array[i][key];
            }
        }
        return undefined;
    }

    render(){

        var unsuccessStyle = {
            color: 'Yellow'
        };
        var successStyle = {
            color: 'Green'
        };
        var warningStyle = {
            color: 'Tomato'
        };

        console.log("rendering ResultItem.js");

        let entityTypeImage="document.png";
        let contentsToShow = null;
        let type = '';

        if (this.props.info.type !== undefined){
            type = this.props.info.type;
        }

        if(type.includes('person')){
            if (this.props.info.source === 'lei'){

                if (this.searchFromJSONArray(this.props.payload.profile,'gender') === 'M'){
                    entityTypeImage = 'male.png';
                } if (this.searchFromJSONArray(this.props.payload.profile,'gender') === 'F'){
                    entityTypeImage = 'female.png';
                }

                contentsToShow = (
                    <div>
                        <h2>
                            <span>{this.searchFromJSONArray(this.props.payload.profile,'given_name1')}&nbsp;</span>
                            <span>{this.searchFromJSONArray(this.props.payload.profile,'given_name2')}&nbsp;</span>
                            <span>{this.searchFromJSONArray(this.props.payload.profile,'last_name')}</span>
                        </h2>
                        <div>ID : {this.props.payload.id === undefined? <span style= {unsuccessStyle}>undefined</span>:<span style= {successStyle}>{this.props.payload.id}</span>}</div>
                        <div>Gender: {this.searchFromJSONArray(this.props.payload.profile,'gender')}</div>
                        <div>DoB: {this.searchFromJSONArray(this.props.payload.profile,'dob')}</div>
                        <div>Height: {this.searchFromJSONArray(this.props.payload.profile,'height')}</div>
                        <div>Eye: {this.searchFromJSONArray(this.props.payload.profile,'colour_eyes_desc')}</div>
                        <div>Weight: {this.searchFromJSONArray(this.props.payload.profile,'weight')}</div>
                        <div>Build: {this.searchFromJSONArray(this.props.payload.profile,'build_desc')}</div>
                        <div>Citizenship:
                            <span>{this.searchFromJSONArray(this.props.payload.profile,'country_citizen_desc')}&nbsp;</span>
                            <span>{this.searchFromJSONArray(this.props.payload.profile,'citizenship_status_desc')}&nbsp;</span>
                        </div>
                        <div>Alerts:
                            <span style= {warningStyle}>
                                {this.searchFromJSONArray(this.props.payload.profile,'alerts_count')}
                            </span>
                        </div>
                        <div>Crime History:
                            <span style= {warningStyle}>
                                {this.searchFromJSONArray(this.props.payload.profile,'crim_histories_count')}
                            </span>
                        </div>
                    </div>
                );
            } else if (this.props.info.source === 'promis'){

                if (this.props.payload.gender === 'M'){
                    entityTypeImage = 'male.png';
                } if (this.props.payload.gender === 'F'){
                    entityTypeImage = 'female.png';
                }

                contentsToShow = (
                    <div>
                        <h2>
                            <span>{this.props.payload.first_name}&nbsp;</span>
                            <span>{this.props.payload.given_name2}&nbsp;</span>
                            <span>{this.props.payload.last_name}</span>
                        </h2>
                        <div>ID : {this.props.payload.id === undefined? <span style= {unsuccessStyle}>undefined</span>:<span style= {successStyle}>{this.props.payload.id}</span>}</div>
                        <div>Gender: {this.props.payload.gender}</div>
                        <div>DoB: {this.props.payload.dob}</div>
                        <div>Height: {this.props.payload.height}</div>
                        <div>Eye: {this.props.payload.colour_eyes}</div>
                        <div>Weight: {this.props.payload.weight}</div>
                        <div>Build: {this.props.payload.build}</div>
                        <div>Citizenship:
                            <span>{this.props.payload.country_citizen}&nbsp;</span>
                            <span>{this.props.payload.citizenship_status}&nbsp;</span>
                        </div>
                    </div>
                );
            }
        }

        if(type.includes('location')){
            entityTypeImage = "location.png";
            if (this.props.info.source === 'lei'){
                contentsToShow = (
                    <div>
                        <h2>
                            <span>{this.searchFromJSONArray(this.props.payload.profile,'street_no')}&nbsp;</span>
                            <span>{this.searchFromJSONArray(this.props.payload.profile,'street_name1')}&nbsp;</span>
                            <span>{this.searchFromJSONArray(this.props.payload.profile,'city_town')}</span>
                        </h2>
                        <div>ID : {this.props.payload.id === undefined? <span style= {unsuccessStyle}>undefined</span>:<span style= {successStyle}>{this.props.payload.id}</span>}</div>
                        <div>Country: {this.searchFromJSONArray(this.props.payload.profile,'country_code_desc')}</div>
                        <div>PCode: {this.searchFromJSONArray(this.props.payload.profile,'pcode')}</div>
                        <div>City/Town: {this.searchFromJSONArray(this.props.payload.profile,'city_town')}</div>
                        <div>Street Type: {this.searchFromJSONArray(this.props.payload.profile,'street_type')}</div>
                        <div>State: {this.searchFromJSONArray(this.props.payload.profile,'state')}</div>
                    </div>
                );
            } else if (this.props.info.source === 'promis'){
                contentsToShow = (
                    <div>
                        <h2>
                            <span>{this.props.payload.state}&nbsp;</span>
                        </h2>
                        <div>ID : {this.props.payload.id === undefined? <span style= {unsuccessStyle}>undefined</span>:<span style= {successStyle}>{this.props.payload.id}</span>}</div>
                        <div>Country: {this.props.payload.city_flag}&nbsp;</div>
                        <div>PCode: {this.props.payload.source_type_desc}&nbsp;</div>
                        <div>City/Town: {this.props.payload.date_time_last_modified}&nbsp;</div>
                        <div>Street Type: {this.props.payload.street_type}&nbsp;</div>
                        <div>State: {this.props.payload.flat_no_type}&nbsp;</div>
                    </div>
                );
            }
        }

        if(type.includes('case')){
            entityTypeImage = "case.png";
            if (this.props.info.source === 'promis'){
                contentsToShow = (
                    <div>
                        <h2>{this.props.payload.case_id}</h2>
                        <div>Incident Type: {this.props.payload.incident_type}</div>
                        <div>Involvement Type: {this.props.payload.involvement_type}</div>
                        <div>Title: {this.props.payload.title}</div>
                        </div>
                );
            }
        }

        if(type.includes('document')){
            entityTypeImage = "document.png";
            contentsToShow = (
                <div>
                    <div>ID : {this.props.payload.id === undefined? <span style= {unsuccessStyle}>undefined</span>:<span style= {successStyle}>{this.props.payload.id}</span>}</div>
                    <div>Format: {this.props.payload.format}</div>
                </div>
            );
        }

        if(type.includes('federated query')){
            let payloadType = this.props.payload.Type;
            if (payloadType != undefined){
                entityTypeImage = ""+payloadType+".png";
            }
            let documentUrl = this.props.payload.Url;
            if (documentUrl != undefined){
                let documentPathSplits = documentUrl.toLocaleString().split("/");
                console.log(documentPathSplits);
                console.log(documentPathSplits.length);

                let documentName = documentPathSplits[documentPathSplits.length-1];
                contentsToShow = (
                    <div>
                        <div>ID : {this.props.payload.ID === undefined? <span style= {unsuccessStyle}>undefined</span>:<span style= {successStyle}>{this.props.payload.ID}</span>}</div>
                        <div>Type : {this.props.payload.Type === 'suspect'?<span style= {warningStyle}>{this.props.payload.Type}</span>:<span style= {successStyle}>{this.props.payload.Type}</span>}</div>
                        <div>Document : <a href={"file:///"+documentUrl}>{documentName}</a></div>
                    </div>
                );
            } else {
                contentsToShow = (
                    <div>
                        <div>ID : {this.props.payload.ID === undefined? <span style= {unsuccessStyle}>undefined</span>:<span style= {successStyle}>{this.props.payload.ID}</span>}</div>
                        <div>Type : {this.props.payload.Type === 'suspect'?<span style= {warningStyle}>{this.props.payload.Type}</span>:<span style= {successStyle}>{this.props.payload.Type}</span>}</div>
                    </div>
                );
            }
        }


        return(
            <div className="row">
                <div className="col-sm-1"/>
                <div className="col-sm-1">
                    <img src={require("components/entitysearch/entity_type_image/"+entityTypeImage)} className="logo" width="100" height="100"/>
                </div>
                <div className="col-sm-9">
                    <strong>{contentsToShow}</strong>
                    <div><br/></div>
                    <div>TYPE : {this.props.info.type}</div>
                    <div>SOURCE : {this.props.info.source}</div>
                    <details>
                        <summary>DETAIL : </summary>
                        <pre>{JSON.stringify(this.props.payload, null, 2) }</pre>
                    </details>
                    <div><br/></div>
                </div>
            </div>
        )

    }
}

module.exports = ResultItem;