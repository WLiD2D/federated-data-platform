package edu.unisa.ILE.FSA.EnginePortal;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.unisa.ILE.FSA.CDMMappingRepository.MRFunctions;
import edu.unisa.ILE.FSA.FederatedDataAccess.OBDAFunctions;
import edu.unisa.ILE.FSA.InternalDataStructure.ControlSpec;
import edu.unisa.ILE.FSA.InternalDataStructure.QuerySpec;
import edu.unisa.ILE.FSA.InternalDataStructure.Request;
import edu.unisa.ILE.FSA.InternalDataStructure.UserAccessSpec;
import org.json.simple.JSONObject;
import org.springframework.web.bind.annotation.*;


import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.simple.JSONObject;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin
@RestController
public class OBDAController {

    @RequestMapping(value = "/obda", method = RequestMethod.POST)
    public
    @ResponseBody
    JSONObject query(@RequestBody JSONObject json) {

        //display the request body
        System.out.println("request body: " + json);

        String existingQuery = (String) json.get("existingQuery");
        String newLabel = (String) json.get("newLabel");

        //fetch result
        JSONObject result = new JSONObject();
        result.put("payload", OBDAFunctions.queryGeneratorVersion1(existingQuery,newLabel));
        return result;
    }
}