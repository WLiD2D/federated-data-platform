package edu.unisa.ILE.FSA.InternalDataStructure;

import org.apache.hadoop.fs.shell.Count;

import javax.validation.constraints.NotNull;

public class CountedName {

//    public static void main(String[] args){
//        CountedName name = new CountedName("test");
//        System.out.println(name.toString());
//        System.out.println(name.increase());
//    }

    private int counter;
    private String prefix;

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public CountedName(String name){
        String[] subs = name.split("(?<=\\D)(?=\\d)");
        this.prefix = subs[0];
        if (subs.length>1){
            counter = Integer.parseInt(subs[1]);
        }
//        System.out.println("prefix:"+this.prefix);
//        System.out.println("counter:"+this.counter);
    }

    public String toString(){
        if (counter == 0){
            return prefix;
        } else {
            return prefix+counter;
        }
    }

    public String increase(){
        counter++;
        return prefix+counter;
    }

    public CountedName clone(){
        CountedName newC = new CountedName(this.getPrefix());
        newC.setCounter(this.getCounter());
        return newC;
    }
}
