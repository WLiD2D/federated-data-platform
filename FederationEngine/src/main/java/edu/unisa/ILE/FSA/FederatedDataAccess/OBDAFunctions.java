package edu.unisa.ILE.FSA.FederatedDataAccess;

import edu.unisa.ILE.FSA.InternalDataStructure.CountedName;
import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.api.core.Term;
import fr.lirmm.graphik.graal.core.DefaultAtom;
import fr.lirmm.graphik.graal.core.DefaultConjunctiveQuery;
import fr.lirmm.graphik.graal.io.dlp.DlgpParser;
import fr.lirmm.graphik.graal.io.dlp.DlgpWriter;
import fr.lirmm.graphik.util.Prefix;
import fr.lirmm.graphik.util.stream.CloseableIteratorWithoutException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class OBDAFunctions {

    public static void main(String[] args) throws Exception {

//        DlgpWriter writer = createDefaultWriter();
//        String testQuery = "?(Address) :- attribute:FN(ID,<John>), attribute:LN(ID,<Smith>), relationship:lives_in(ID,Y), attribute:Address(Y, Address).";
////        queryGenerator("?(Address) :- attribute:FN(ID,<John>), attribute:LN(ID,<Smith>), relationship:lives_in(ID,Y), attribute:Address(Y, Address).",null);
////        queryGenerator("?(VID, RegNo) :- attribute:givenName(PID,<John>), attribute:hasRegNo(VID,RegNo), relationship:uses(PID,VID).\n",null);
////        String atomIdString = "http://unisa.edu.au/KSE.owl/attribute#givenName";
////        String prefix = atomIdString.split("#")[0];
//
//
//        String[] subStringArray = testQuery.split("\\),");
//        String lastAtomString = subStringArray[subStringArray.length-1].replace(".","").trim();
//        String lastAtomType = lastAtomString.split(":")[0];
//        String[] lastAtomTerms = lastAtomString.substring(lastAtomString.lastIndexOf("(")+1).replace(")","").split(",");
//        for (int i=0;i<lastAtomTerms.length;i++){
//            lastAtomTerms[i] = lastAtomTerms[i].trim();
//        }
//        for (int i=0;i<lastAtomTerms.length;i++){
//            System.out.println(lastAtomTerms[i]);
//        }
        String existingQuery = "?(ID,Type) :- class:Contact(ID,Type),relationship:Test(ID1, ID2, Type1).";
        String newLabel = "class:Test(ID,Type)";
        String output = queryGeneratorVersion1(existingQuery, newLabel);
        formAnswerVariables(output);
        System.out.println(output);
    }

    public static String queryGeneratorVersion1(String existingQueryString, String newLabel) {

        CountedName lastAtomID;
        CountedName lastAtomType;
        String lastAtomPrifix;
        String query;

        String newAtomPrifix = newLabel.split(":")[0];
        String newAtomPredicate = newLabel.split(":")[1].split("\\(")[0];
        String[] newAtomTerms = newLabel.substring(newLabel.lastIndexOf("(")+1).replace(")","").split(",");
        for (int i=0;i<newAtomTerms.length;i++){
            newAtomTerms[i] = newAtomTerms[i].trim();
        }
        CountedName newAtomID = new CountedName(newAtomTerms[0]);
        CountedName newAtomType = new CountedName(newAtomTerms[newAtomTerms.length-1]);

        if (existingQueryString != ""){
            query = existingQueryString.split(":-")[1];
            if (query.endsWith(".")){
                query = query.replace(".","");
            }
            //if exist previous query string, merge the new label
            String[] subStringArray = query.split("\\),");
            String lastAtomString = subStringArray[subStringArray.length-1].replace(".","").trim();
            lastAtomPrifix = lastAtomString.split(":")[0];
            String[] lastAtomTerms = lastAtomString.substring(lastAtomString.lastIndexOf("(")+1).replace(")","").split(",");
            for (int i=0;i<lastAtomTerms.length;i++){
                lastAtomTerms[i] = lastAtomTerms[i].trim();
            }
            if (lastAtomPrifix.equals("class")){
                lastAtomID = new CountedName(lastAtomTerms[0]);
                lastAtomType = new CountedName(lastAtomTerms[1]);
                if (newAtomPrifix.equals("attribute")){
                    newAtomID = lastAtomID.clone();
                    newAtomType = lastAtomType.clone();
                    newAtomTerms[0] = newAtomID.toString();
                    newAtomTerms[1] = newAtomPredicate;
                    newAtomTerms[2] = newAtomType.toString();
                    query += ", "+ buildAtomString(newAtomPrifix,newAtomPredicate,newAtomTerms);
                } else if(newAtomPrifix.equals("relationship")){
                    newAtomID = lastAtomID.clone();
                    newAtomTerms[0] = newAtomID.toString();
                    newAtomTerms[1] = newAtomID.increase();
                    newAtomTerms[2] = lastAtomType.increase();
                    System.out.println(buildAtomString(newAtomPrifix,newAtomPredicate,newAtomTerms));
                    query += ", "+ buildAtomString(newAtomPrifix,newAtomPredicate,newAtomTerms);
                } else if (newAtomPrifix.equals("class")){
                    //do nothing
                }
            } else if (lastAtomPrifix.equals("attribute")){
                lastAtomID = new CountedName(lastAtomTerms[0]);
                lastAtomType = new CountedName(lastAtomTerms[2]);
                if (newAtomPrifix.equals("attribute")){
                    newAtomID = lastAtomID.clone();
                    newAtomType = lastAtomType.clone();
                    newAtomTerms[0] = newAtomID.toString();
                    newAtomTerms[1] = newAtomPredicate;
                    newAtomTerms[2] = newAtomType.toString();
                    query += ", "+ buildAtomString(newAtomPrifix,newAtomPredicate,newAtomTerms);
                } else if(newAtomPrifix.equals("relationship")){
                    newAtomID = lastAtomID.clone();
                    newAtomTerms[0] = newAtomID.toString();
                    newAtomTerms[1] = newAtomID.increase();
                    newAtomTerms[2] = lastAtomType.increase();
                    query += ", "+ buildAtomString(newAtomPrifix,newAtomPredicate,newAtomTerms);
                } else if (newAtomPrifix.equals("class")){

                }
            } else if (lastAtomPrifix.equals("relationship")){
                lastAtomID = new CountedName(lastAtomTerms[1]);
                lastAtomType = new CountedName(lastAtomTerms[2]);
                if (newAtomPrifix.equals("attribute")){
                    newAtomID = lastAtomID.clone();
                    newAtomTerms[0] = newAtomID.toString();
                    newAtomTerms[2] = lastAtomType.increase();
                    query += ", "+ buildAtomString(newAtomPrifix,newAtomPredicate,newAtomTerms);
                } else if(newAtomPrifix.equals("relationship")){

                } else if (newAtomPrifix.equals("class")){
                    newAtomID = lastAtomID.clone();
                    newAtomTerms[0] = newAtomID.toString();
                    newAtomTerms[1] = lastAtomType.increase();
                    query += ", "+ buildAtomString(newAtomPrifix,newAtomPredicate,newAtomTerms);
                }
            }

            HashSet<String> answerVariables = formAnswerVariables(query);
            String answerPart = "?(";
            Iterator<String> it = answerVariables.iterator();
            while(it.hasNext()){
                String term = it.next();
                answerPart += term;
                if (it.hasNext()){
                    answerPart += ",";
                } else {
                    answerPart += ")";
                }
            }
            query = answerPart + " :- "+query;
        } else {
            //no existing query string, need to form a new one using the new label
            query = "?(";
            for (int i = 0; i<newAtomTerms.length; i++){
                if (i==newAtomTerms.length-1){
                    query+=newAtomTerms[i];
                } else {
                    query+=newAtomTerms[i]+",";
                }
            }
            query +=") :- "+buildAtomString(newAtomPrifix,newAtomPredicate,newAtomTerms);
        }





        if (!query.endsWith(".")){
            query += ".";
        }
        return query;
    }

    public static HashSet<String> formAnswerVariables(String queryString){
        if (queryString.endsWith(".")){
            queryString = queryString.replace(".","");
        }

        HashSet<String> answerVariables = new HashSet<>();

        String[] atoms = queryString.split("\\),");
        for (String atom:atoms){
            String[] terms = atom.substring(atom.indexOf("(")+1).split(",");
            for (String term:terms){
                term = term.replace(")","").trim();
                answerVariables.add(term);
            }
        }
        return answerVariables;
    }

    public static String buildAtomString(String prefix, String predicate, String[] terms){
        String atomString = prefix + ":" + predicate+"(";
        for (int i = 0; i<terms.length; i++){
            if (i==terms.length-1){
                atomString+=terms[i]+")";
            } else {
                atomString+=terms[i]+",";
            }
        }
        return atomString;
    }

    public static ConjunctiveQuery buildQuery(String query) throws Exception {
        String prefixes = "@prefix class: <http://unisa.edu.au/KSE.owl/class#>\n"
                + "        @prefix attribute: <http://unisa.edu.au/KSE.owl/attribute#>\n"
                + "        @prefix relationship: <http://unisa.edu.au/KSE.owl/relationship#>\n"
                + "        @prefix : <>\n";
        String wholeString = prefixes + query;
        ConjunctiveQuery conjunctiveQuery = DlgpParser.parseQuery(wholeString);
        return conjunctiveQuery;
    }

    public static Atom buildAtom(String atomString) throws Exception {
        String prefixes = "@prefix class: <http://unisa.edu.au/KSE.owl/class#>\n"
                + "        @prefix attribute: <http://unisa.edu.au/KSE.owl/attribute#>\n"
                + "        @prefix relationship: <http://unisa.edu.au/KSE.owl/relationship#>\n"
                + "        @prefix : <>\n";
        String wholeString = prefixes + atomString;
        if (!wholeString.endsWith(".")){
            wholeString += ".";
        }
        Atom atom = DlgpParser.parseAtom(wholeString);
        return atom;
    }

    public static DlgpWriter createDefaultWriter() throws Exception {
        DlgpWriter writer = new DlgpWriter();
        writer.write(new Prefix("class", "http://unisa.edu.au/KSE.owl/class#"));
        writer.write(new Prefix("attribute", "http://unisa.edu.au/KSE.owl/attribute#"));
        writer.write(new Prefix("relationship", "http://unisa.edu.au/KSE.owl/relationship#"));
        writer.write(new Prefix("", ""));
        return writer;
    }
}
